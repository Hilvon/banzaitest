using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleWeapon : Weapon
{
    [SerializeField]
    private Transform _ProjectileSpawnPoint;

    protected override Transform GetProjectileSpawnPoint() {
        return _ProjectileSpawnPoint;
    }
}
