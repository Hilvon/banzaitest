using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ProjectileBase : MonoBehaviour
{
    [SerializeField]
    protected LayerMask _ValidTargets;

    public event Action<GameObject, float> OnHitDelivered;
    public event Action<ProjectileBase> ProjectileDespawning;

    protected void DeliverHit(GameObject target, float damagePct = 1) {
        OnHitDelivered?.Invoke(target, damagePct);
    }

    protected void TerminateProjectile() {
        ProjectileDespawning?.Invoke(this);
    }
    public void Reset() {
        OnHitDelivered = null;
        ProjectileDespawning = null;
    }
    public virtual void Launch() { }
}
