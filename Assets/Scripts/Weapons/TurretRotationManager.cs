using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretRotationManager : MonoBehaviour
{
    [SerializeField]
    private WeaponSelectionManager _WeaponManager;

    private Camera __CachedCamera;
    private Camera _CachedCamera {
        get {
            if (__CachedCamera == null)
                __CachedCamera = Camera.main;
            return __CachedCamera;
        }
    }

    private LayerMask __CachedMask;
    private LayerMask _CachedMask {
        get {
            if (__CachedMask.value == 0)
                __CachedMask = LayerMask.GetMask("Ground");
            return __CachedMask;
        }
    }
    // Update is called once per frame
    void Update()
    {
        var targetDirection = PickTargetRotation();
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetDirection, 70 * Time.deltaTime);
            
    }

    private Quaternion PickTargetRotation() {
        if (_WeaponManager.IsCurrentWeaponRotatable)
            if (Physics.Raycast(_CachedCamera.ScreenPointToRay(Input.mousePosition), out var hitInfo, 999, _CachedMask))
                return Quaternion.LookRotation(Orthogonize(hitInfo.point - transform.position, transform.up), transform.up);
        return transform.parent.rotation;
    }

    private static Vector3 Orthogonize(Vector3 direction, Vector3 up) {
        var norm = up.normalized;
        return direction - norm * Vector3.Dot(direction, norm);
    }

}
