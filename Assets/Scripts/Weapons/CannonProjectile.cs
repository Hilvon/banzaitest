using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonProjectile : ProjectileBase
{
    [SerializeField]
    private float _TravelSpeed;
    [SerializeField]
    private float _HitRadius;

    private void Update() {
        if (Physics.SphereCast(transform.position - transform.forward * 0.5f, _HitRadius, transform.forward, out var hitiInfo, _TravelSpeed * Time.deltaTime + 0.5f, _ValidTargets)) {
            DeliverHit(hitiInfo.collider.gameObject);
            TerminateProjectile();
        }
        transform.position += transform.forward * _TravelSpeed * Time.deltaTime;
    }

    public override void Launch() {
        base.Launch();
        transform.parent = null;
    }
}
