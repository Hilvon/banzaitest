using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingMissleProjectile : ProjectileBase
{
    private static Camera __CachedCamera;
    private static Camera _CachedCamera {
        get {
            if (__CachedCamera == null)
                __CachedCamera = Camera.main;
            return __CachedCamera;
        }
    }

    [SerializeField]
    private LayerMask _ValidTargetsMask;
    [SerializeField]
    private float _Radius;
    [SerializeField]
    private float _TravelSpeed;
    [SerializeField]
    private float _RotationSpeed;
    [SerializeField]
    private float _SuperRotationThreshold;


    private Vector3 _HomingTarget;
    private float DistanceTravelled;
    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(_CachedCamera.ScreenPointToRay(Input.mousePosition), out var hitInfo, 999, _ValidTargetsMask)) {
            _HomingTarget = hitInfo.point+Vector3.up*0.5f;
        }
        var direction = _HomingTarget - transform.position;
        var requiredHeading = Quaternion.LookRotation(direction, Vector3.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, requiredHeading, direction.sqrMagnitude > _SuperRotationThreshold ? _RotationSpeed * Time.deltaTime : 180);
        if (Physics.SphereCast(transform.position, _Radius, transform.forward, out var hitInfo2, _TravelSpeed * Time.deltaTime, _ValidTargets)) {
            DeliverHit(hitInfo2.collider.gameObject);
            TerminateProjectile();
        }
        transform.position += transform.forward * _TravelSpeed * Time.deltaTime;
    }

    public override void Launch() {
        base.Launch();
        transform.SetParent(null);
        DistanceTravelled = 0f;
    }
}
