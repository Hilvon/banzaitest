using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WeaponSelectionManager : MonoBehaviour
{
    private Weapon[] _WeaponOptions;
    private int _CelectedWeaponIndex;

    public bool IsCurrentWeaponRotatable => _WeaponOptions[_CelectedWeaponIndex].IsRotatable;
    private void Start()
    {
        _CelectedWeaponIndex = 0;
        _WeaponOptions = Resources.LoadAll<Weapon>("Prefabs/Weapons").Select(_=>Instantiate(_, transform, false)).ToArray();
        foreach (var weap in _WeaponOptions) {
            weap.gameObject.SetActive(false);
        }
        _WeaponOptions[_CelectedWeaponIndex].gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetAxisRaw("WeaponShift") > 0.9f)
            ShiftActiveWeapon(1);
        else if (Input.GetAxisRaw("WeaponShift") <-0.9f)
            ShiftActiveWeapon(-1);
        else
            RepeatLock = false;
    }
    private bool RepeatLock = false;
    private void ShiftActiveWeapon(int shift) {
        if (RepeatLock)
            return;
        _WeaponOptions[_CelectedWeaponIndex].gameObject.SetActive(false);
        _CelectedWeaponIndex = (_CelectedWeaponIndex + shift + _WeaponOptions.Length) % _WeaponOptions.Length;
        _WeaponOptions[_CelectedWeaponIndex].gameObject.SetActive(true);
        RepeatLock = true;
    }
}
