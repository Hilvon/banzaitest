using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultibarrelWeapon : Weapon
{
    [SerializeField]
    private Transform[] _ProjectileSpawnPoint;


    private int _CurrentBarrelIndex;
    protected override Transform GetProjectileSpawnPoint() {
        _CurrentBarrelIndex = (_CurrentBarrelIndex + 1) % _ProjectileSpawnPoint.Length;
        return _ProjectileSpawnPoint[_CurrentBarrelIndex];
    }
}
