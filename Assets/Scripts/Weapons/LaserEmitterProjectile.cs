using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEmitterProjectile : ProjectileBase
{
    [SerializeField]
    private float _Duration;
    [SerializeField]
    private LineRenderer _BeamRenderer;

    private float _BurnTime;

    void Update()
    {
        if (_BurnTime > Time.deltaTime) {
            _BurnTime -= Time.deltaTime;
            HandleBeam(Time.deltaTime);
        }
        else {
            HandleBeam(_BurnTime);
            _BurnTime = 0;
            TerminateProjectile();
        }
    }

    private void HandleBeam(float beamPct) {
        if (Physics.Raycast(transform.position, transform.forward, out var hitInfo, 999, _ValidTargets)) {
            DeliverHit(hitInfo.collider.gameObject, beamPct / _Duration);
            _BeamRenderer.SetPosition(1, transform.InverseTransformPoint(hitInfo.point));
        }
        else {
            _BeamRenderer.SetPosition(1, transform.InverseTransformPoint(transform.position + transform.forward * 999));
        }
    }

    public override void Launch() {
        _BurnTime = _Duration;
    }
}
