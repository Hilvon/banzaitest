using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public bool IsRotatable => _IsRotatable;
    [SerializeField]
    private bool _IsRotatable;

    protected abstract Transform GetProjectileSpawnPoint();

    [SerializeField]
    private ProjectileBase _ProjectileTemplate;

    [SerializeField]
    private float _Damage;

    [SerializeField]
    private float _Cooldown;

    [SerializeField]
    private PoolingService _Pools;

    private bool _RepeatLock;
    private float _CDTimer;
    // Update is called once per frame
    void Update()
    {
        _CDTimer -= Time.deltaTime;
        if (Input.GetAxis("Fire1") > 0.9f) {
            if (_CDTimer <= 0) {
                if (!_RepeatLock) {
                    Debug.Log($"Shooting {name}");
                    _CDTimer = _Cooldown;
                    var newProjctile = FetchProjectile();
                    newProjctile.gameObject.SetActive(false);
                    var spawnPoint = GetProjectileSpawnPoint();
                    newProjctile.transform.SetParent(spawnPoint);
                    newProjctile.transform.SetPositionAndRotation(spawnPoint.position, spawnPoint.rotation);
                    newProjctile.Reset();
                    newProjctile.OnHitDelivered += OnProjectileHit;
                    newProjctile.ProjectileDespawning += OnProjectileTerminated;
                    newProjctile.gameObject.SetActive(true);
                    newProjctile.Launch();
                    _RepeatLock = true;
                }
            }
        }
        else {
            _RepeatLock = false;
        }
    }

    private void OnProjectileHit(GameObject target, float damagePct) {
        target.GetComponentInParent<IDamageable>()?.GetDamaged(new DamageDescriptor(_Damage * damagePct));
    }
    private void OnProjectileTerminated(ProjectileBase projectile) {
        projectile.Reset();
        _Pools.StoreItem(name, projectile);
    }

    private void OnDestroy() {
        _Pools.FlushStash<ProjectileBase>(name);
    }

    private ProjectileBase FetchProjectile() {
        return _Pools.TryGetStashedObject<ProjectileBase>(name) ?? Instantiate(_ProjectileTemplate);

    }
}
