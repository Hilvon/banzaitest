using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/PoolingService")]
public class PoolingService : ScriptableObject
{
    private Transform __PooledItemRoot;
    private Transform _PooledItemRoot {
        get {
            if (__PooledItemRoot == null) {
                var rootObject = new GameObject("Pool");
                DontDestroyOnLoad(rootObject);
                rootObject.SetActive(false);
                __PooledItemRoot = rootObject.transform;
            }
            return __PooledItemRoot;
        }
    }

    private Dictionary<string, object> _Stashes = new Dictionary<string, object>();

    public void StoreItem<T>(string poolName, T item) where T:MonoBehaviour{
        var stashName = $"{typeof(T).Name}<{poolName}";
        if (_Stashes.TryGetValue(stashName, out var stash)) {
            var typedStash = stash as Stack<T>;
            if (typedStash != null) {
                typedStash.Push(item);
                item.transform.SetParent(_PooledItemRoot);
                return;
            } else {
                throw new System.Exception("Pool was not of appropriate type. Potential name conflict with another pool");
            }
        }
        var newStash = new Stack<T>();
        newStash.Push(item);
        item.transform.SetParent(_PooledItemRoot);
        _Stashes.Add(stashName, newStash);
    }

    public T TryGetStashedObject<T>(string poolName) where T : MonoBehaviour {
        if (_Stashes.TryGetValue($"{typeof(T).Name}<{poolName}", out var stash)) {
            var typedStash = stash as Stack<T>;
            if (typedStash != null) {
                while (typedStash.Count > 0) {
                    var candidate = typedStash.Pop();
                    if (candidate != null)
                        return candidate;
                }
            }
        }
        return null;
    }
    public void FlushStash<T>(string poolName) where T : MonoBehaviour {
        if (_Stashes.TryGetValue($"{typeof(T).Name}<{poolName}", out var stash)) {
            var typedStash = stash as Stack<T>;
            if (typedStash != null) {
                while (typedStash.Count>0) {
                    var item = typedStash.Pop();
                    if (item != null)
                        Destroy(item.gameObject);
                }
            }
        }
    }
}
