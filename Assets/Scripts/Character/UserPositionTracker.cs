using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Game/PositionTracker")]
public class UserPositionTracker : ScriptableObject
{
    public void ReportPosition(Vector3 position) {
        CurrentPosition = position;
    }
    public Vector3 CurrentPosition { get; private set; }
}
