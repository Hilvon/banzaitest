using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidBodyMoveHandler : MovementHandler
{
    [SerializeField]
    private Rigidbody _Controller;

    public override void SetMoveSpeed(Vector3 worldspaceSpeed) {
        if (_Controller == null)
            _Controller = GetComponent<Rigidbody>();

        _Controller.velocity = new Vector3(worldspaceSpeed.x, _Controller.velocity.y, worldspaceSpeed.z);        
    }
}
