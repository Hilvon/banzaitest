using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMovementSource : UserInputSource
{
    public override Vector2 CurrentInput => new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
}
