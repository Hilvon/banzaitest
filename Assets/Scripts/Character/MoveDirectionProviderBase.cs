using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MoveDirectionProviderBase : MonoBehaviour
{
    public abstract Vector2 ActiveDirection { get; }
}
