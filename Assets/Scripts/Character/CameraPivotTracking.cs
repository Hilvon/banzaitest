using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPivotTracking : MonoBehaviour
{
    [SerializeField]
    private Transform _TargetPosition;
    [SerializeField]
    private float _Speed;

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, _TargetPosition.position, Time.deltaTime * _Speed);
    }
}
