using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public enum CalculationMode { Simple, Angular }

    private Vector3 _CurrentMotion;

    [SerializeField]
    private float _Acceleration;
    [SerializeField]
    private float _Speed;

    [SerializeField]
    private MovementHandler _MoveProvider;
    [SerializeField]
    private MoveDirectionProviderBase _DirectionSource;
    [SerializeField]
    private CalculationMode _CalculationMode;
    void Update() {
        var desiredSpeed = _DirectionSource.ActiveDirection.Unflatten() * _Speed;

        switch (_CalculationMode) {
            case CalculationMode.Angular:
                _CurrentMotion = AngularCalculation(desiredSpeed);
            break;
            default:
                _CurrentMotion = SimpleCalculation(desiredSpeed);
            break;
        }
        
        if (_CurrentMotion.sqrMagnitude > 0.01f) {
            _MoveProvider.SetMoveSpeed(_CurrentMotion);
        }
    }

    private Vector3 SimpleCalculation(Vector3 desiredSpeed) {
        var newMovement = Vector3.MoveTowards(_CurrentMotion, desiredSpeed, _Acceleration * Time.deltaTime);
        if (newMovement.sqrMagnitude>0.01f)
            transform.rotation = Quaternion.LookRotation(newMovement, Vector3.up);
        return newMovement;
    }

    private Vector3 AngularCalculation(Vector3 desiredSpeed) {
        var (desiredAngle, desiredVelovity) = (desiredSpeed.sqrMagnitude > 0.01f) ? ToAngular(desiredSpeed) : (transform.rotation.eulerAngles.y, 0);

        var (currentAngle, currentVelocity) = (_CurrentMotion.sqrMagnitude > 0.01f) ? ToAngular(_CurrentMotion) : (transform.rotation.eulerAngles.y, 0);

        currentVelocity = Mathf.Lerp(currentVelocity, desiredVelovity * Mathf.Clamp01(Mathf.Cos(Mathf.Deg2Rad * (desiredAngle - currentAngle))), _Acceleration * Time.deltaTime);
        currentAngle = Mathf.MoveTowardsAngle(currentAngle, desiredAngle, 60 * Time.deltaTime);
        
        transform.rotation = Quaternion.Euler(0, currentAngle, 0);
        return ToDirection(currentAngle, currentVelocity);
    }

    private static (float, float) ToAngular(Vector3 direction) {
        return (Quaternion.LookRotation(direction, Vector3.up).eulerAngles.y, //angle
            direction.magnitude); //size;
    }

    private static Vector3 ToDirection(float angle, float Speed) {
        return Quaternion.Euler(0, angle, 0) * Vector3.forward * Speed;
    }
}
