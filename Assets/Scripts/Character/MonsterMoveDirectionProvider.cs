using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterMoveDirectionProvider : MoveDirectionProviderBase
{
    [SerializeField]
    private UserPositionTracker _PlayerPositionTracker;
    [SerializeField]
    private float _RefreshRateCap = 0; //In case we do need many enemies, we might want to cap the rate at which the enemies refresh their navigation to save performance.

    private Vector2 _CurrentDirection;

    private bool _InSightMode;
    private float _Cooldown;
    private NavMeshPath _CurrentPath;
    private int NextCornerIndex;
    private static int __PlayerLayer;
    private static int _PalyerLayer {
        get {
            if (__PlayerLayer == 0)
                __PlayerLayer = LayerMask.NameToLayer("Player");
            return __PlayerLayer;
        }
    }
    public override Vector2 ActiveDirection => _CurrentDirection;

    private void Start() {
        _Cooldown = Random.Range(0, _RefreshRateCap); //Randomize initial cooldown to awoid having all providers to update on same frame.
    }
    // Update is called once per frame
    private void Update() {
        _Cooldown -= Time.unscaledDeltaTime;
        if (_Cooldown <= 0) {
            _Cooldown = _RefreshRateCap * Random.Range(0.9f, 1.1f); //Randomizing cooldowns to prevent accidental tickers syncing forever.
            Debug.Log($"have hit: {Physics.Raycast(transform.position, (_PlayerPositionTracker.CurrentPosition - transform.position), out var hitinfo2)} layerMatch: {_PalyerLayer == hitinfo2.collider.gameObject.layer} {hitinfo2.collider.name}");
            if (!Physics.Raycast(transform.position, (_PlayerPositionTracker.CurrentPosition - transform.position), out var hitinfo) || _PalyerLayer == hitinfo.collider.gameObject.layer) {
                _CurrentDirection = (_PlayerPositionTracker.CurrentPosition - transform.position).Flatten().normalized;
                if (_CurrentPath != null) {
                    _CurrentPath.ClearCorners();
                    _CurrentPath = null;
                }
            }
            else {
                //Line of sight broken. Switshing to navigator mode;
                _CurrentPath = new NavMeshPath();
                NavMesh.CalculatePath(transform.position, _PlayerPositionTracker.CurrentPosition, NavMesh.AllAreas, _CurrentPath);
                NextCornerIndex = 1;
                _CurrentDirection = (_CurrentPath.corners[NextCornerIndex] - transform.position).Flatten().normalized;
            }
        }
        if (_CurrentPath != null) {
            if (Vector2.Dot(_CurrentDirection, (_CurrentPath.corners[NextCornerIndex] - transform.position).Flatten())<=0) {
                NextCornerIndex++;
                if (_CurrentPath.corners.Length > NextCornerIndex) {
                    _CurrentDirection = (_CurrentPath.corners[NextCornerIndex] - transform.position).Flatten().normalized;
                }
                else { 
                    // This is wierd. Ran out of the path without colliding with the player, or at least getting into view. Do nothing and wait for a refresh.
                }
            }
        }
    }
}
