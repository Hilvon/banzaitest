using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawningService : MonoBehaviour
{
    [SerializeField]
    private PoolingService _PoolingService;
    [SerializeField]
    private float _SpawnDistance;

    private MonsterDeathManager[] _MonsterPrefabs;

    public int SpawnIfLessThan = 10;

    private HashSet<MonsterDeathManager> _SpawnedMonsters = new HashSet<MonsterDeathManager>();
    private Dictionary<string, System.Action<MonsterDeathManager>> _Despawners = new Dictionary<string, System.Action<MonsterDeathManager>>();

    private System.Action<MonsterDeathManager> GetDespawner(string name) {
        if (_Despawners.TryGetValue(name, out var despawner))
            return despawner;
        System.Action<MonsterDeathManager> newDespawner = monster => {
            _PoolingService.StoreItem(name, monster);
            _SpawnedMonsters.Remove(monster);
        };
        _Despawners.Add(name, newDespawner);
        return newDespawner;
    }
    // Update is called once per frame
    private void Update()
    {
        while (_SpawnedMonsters.Count < SpawnIfLessThan)
            SpawnExtra();
    }

    private void Start() {
        _MonsterPrefabs = Resources.LoadAll<MonsterDeathManager>("Prefabs/Monsters");
    }

    private void SpawnExtra() {
        var newMonster = GetMonster();
        newMonster.transform.position = transform.position + Random.insideUnitCircle.Unflatten().normalized * _SpawnDistance;
        newMonster.Reset();
        _SpawnedMonsters.Add(newMonster);
    }

    private MonsterDeathManager GetMonster() {
        var prefab = _MonsterPrefabs[Random.Range(0, _MonsterPrefabs.Length)];
        return _PoolingService.TryGetStashedObject<MonsterDeathManager>(prefab.name) ?? InstantiateMonster(prefab);
    }

    private MonsterDeathManager InstantiateMonster(MonsterDeathManager prefab) {
        var newMonster = Instantiate(prefab);
        newMonster.OnMonsterKilled += GetDespawner(prefab.name);
        return newMonster;
    }


}
