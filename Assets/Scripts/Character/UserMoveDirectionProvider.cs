using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserMoveDirectionProvider : MoveDirectionProviderBase
{
    [SerializeField]
    private UserInputManager _UserInputManager; //Should be handled via DI, but for brievity will use Scriptlable object.

    public override Vector2 ActiveDirection => _UserInputManager.CollectUserInput();
}
