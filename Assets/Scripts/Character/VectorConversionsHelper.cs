using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorConversionsHelper
{
    public static Vector2 Flatten(this Vector3 direction) {
        return new Vector2(direction.x, direction.z);
    }

    public static Vector3 Unflatten(this Vector2 direction) {
        return new Vector3(direction.x, 0, direction.y);
    }
}
