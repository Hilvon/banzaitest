using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharControllerMoveHandler : MovementHandler
{
    [SerializeField]
    private CharacterController _Controller;

    public override void SetMoveSpeed(Vector3 worldspaceSpeed) {
        if (_Controller == null)
            _Controller = GetComponent<CharacterController>();
        _Controller.SimpleMove(worldspaceSpeed);
    }
}
