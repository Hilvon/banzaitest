using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementHandler : MonoBehaviour
{
    public abstract void SetMoveSpeed(Vector3 worldspaceSpeed);
}
