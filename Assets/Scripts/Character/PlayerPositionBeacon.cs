using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionBeacon : MonoBehaviour
{
    [SerializeField]
    private UserPositionTracker _Tracker;
    private void Update()
    {
        _Tracker.ReportPosition(transform.position);
    }
}
