using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UserInputSource : MonoBehaviour
{
    [SerializeField]
    private UserInputManager _Manager;
    public abstract Vector2 CurrentInput { get; }

    private void OnEnable() {
        _Manager.RegisterSource(this);
    }
    private void OnDisable() {
        _Manager.UnregisterSource(this);
    }
}
