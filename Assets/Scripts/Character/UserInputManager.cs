using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class can and should be handled as a service provided via DI. Making it scriptable object is a compromise.

[CreateAssetMenu(menuName ="Game/UserInputManager")]
public class UserInputManager : ScriptableObject
{
    private List<UserInputSource> _RegisterredSources;

    public void RegisterSource(UserInputSource source) {
        _RegisterredSources.Add(source);
    }

    public void UnregisterSource(UserInputSource source) {
        _RegisterredSources.Remove(source);
    }

    public Vector2 CollectUserInput() {
        var activeSourcesNumber = 0;
        var accumulatedInput = Vector2.zero;
        foreach (var source in _RegisterredSources) { 
            if (source.enabled) {
                var input = source.CurrentInput;
                if (input.sqrMagnitude>=0.01f) {
                    activeSourcesNumber++;
                    accumulatedInput += input;
                }
            }
        }
        if (activeSourcesNumber > 0) {
            accumulatedInput /= activeSourcesNumber;
        }
        return accumulatedInput;
    }
}
