using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactDetonation : MonoBehaviour, IDeathReasonProvider
{
    [SerializeField]
    private LayerMask _ValidTargets;

    [SerializeField]
    private PayloadBase _Payload;

    public event Action DeathPronounced;

    private void OnTriggerEnter(Collider other) {
        Debug.Log($"Contact! {other.name} is valid contact: {(_ValidTargets.value & (1 << other.gameObject.layer)) != 0}");
        if ((_ValidTargets.value & (1<<other.gameObject.layer)) != 0) {
            DeathPronounced?.Invoke();
            other.gameObject.GetComponentInParent<IDamageable>()?.GetDamaged(_Payload.Damage);
        }
    }
}
