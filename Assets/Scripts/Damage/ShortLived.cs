using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortLived : MonoBehaviour, IDeathReasonProvider 
{
    [SerializeField]
    private float _Lifespan;

    public event Action DeathPronounced;
    private void Start()
    {
        StartCoroutine(ProcessLifespan());
    }

    private IEnumerator ProcessLifespan() {
        yield return new WaitForSeconds(_Lifespan);
        DeathPronounced?.Invoke();
    }
}
