using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterDeathManager : DeathManager
{
    public event Action<MonsterDeathManager> OnMonsterKilled;
    [SerializeField]
    private Health _Health;

    public void Reset() {
        _Health.Reset();
    }
    protected override void HandleDying() {
        OnMonsterKilled?.Invoke(this);

        Destroy(gameObject);
    }
}
