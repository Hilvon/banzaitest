using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DeathManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _RemnantTemplate;

    private IDeathReasonProvider[] _RegisterredProviders;
    // Start is called before the first frame update
    void Start()
    {
        _RegisterredProviders = GetComponentsInChildren<IDeathReasonProvider>(true);
        foreach (var reasonProvider in _RegisterredProviders) {
            reasonProvider.DeathPronounced += HandleDying;
        }
    }

    private void OnDestroy() {
        foreach (var reasonProvider in _RegisterredProviders) {
            reasonProvider.DeathPronounced -= HandleDying;
        }
    }

    protected abstract void HandleDying();
    //{
    //    if (_RemnantTemplate != null) {
    //        Instantiate(_RemnantTemplate, transform.position, transform.rotation, transform.parent);
    //    }
    //    Destroy(gameObject);
    //}
}
