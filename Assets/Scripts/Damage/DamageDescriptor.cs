using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DamageDescriptor
{
    public readonly float DamageAmount;

    public DamageDescriptor(float damageAmount) {
        this.DamageAmount = damageAmount;
    }
}
