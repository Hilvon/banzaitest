using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathManager : DeathManager
{
    [SerializeField]
    private Health _Health;

    private void Start() {
        _Health.Reset();
    }
    protected override void HandleDying() {
        Debug.Log("PlayerDead! Game over. Game will restart now.");

        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
