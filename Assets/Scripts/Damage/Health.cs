using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Health : MonoBehaviour, IDamageable, IDeathReasonProvider 
{
    [SerializeField]
    private float _CurrentDamageReduction;
    [SerializeField]
    private float _StartingHealth;

    private float _CurrentHealth;
    public event Action OnHealthDepleted;

    event Action IDeathReasonProvider.DeathPronounced {
        add {
            OnHealthDepleted += value;
        }

        remove {
            OnHealthDepleted -= value;
        }
    }
    public void Reset() {
        _CurrentHealth = _StartingHealth;
    }
    void IDamageable.GetDamaged(DamageDescriptor damage) {
        _CurrentHealth -= damage.DamageAmount * (1-_CurrentDamageReduction);
        if (_CurrentHealth <= 0) {
            OnHealthDepleted?.Invoke();
        }
    }
}
