using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PayloadBase : MonoBehaviour
{
    public abstract DamageDescriptor Damage { get; }
}
