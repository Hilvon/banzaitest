using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterPayload : PayloadBase
{
    [SerializeField]
    private float _CollisionDamage;
    public override DamageDescriptor Damage => new DamageDescriptor(_CollisionDamage);
}
